${AnsiColor.BRIGHT_GREEN}
   ___                _     _
  / _ \_ __ _____   _(_) __| | ___ _ __
 / /_)/ '__/ _ \ \ / / |/ _` |/ _ \ '__|
/ ___/| | | (_) \ V /| | (_| |  __/ |
\/    |_|  \___/ \_/ |_|\__,_|\___|_|


${AnsiColor.BRIGHT_MAGENTA}::${AnsiColor.BRIGHT_WHITE} Spring Boot Version: ${AnsiColor.BRIGHT_YELLOW}${spring-boot.version}${spring-boot.formatted-version} ${AnsiColor.BRIGHT_MAGENTA}::
::${AnsiStyle.FAINT} https://gitee.com/darkranger/spring-cloud-books/tree/master/spring-cloud-provider-book ${AnsiColor.BRIGHT_MAGENTA}::
${AnsiColor.BRIGHT_WHITE}