${AnsiColor.BRIGHT_GREEN}
                  _        _
  /\  /\_   _ ___| |_ _ __(_)_  __
 / /_/ / | | / __| __| '__| \ \/ /
/ __  /| |_| \__ \ |_| |  | |>  <
\/ /_/  \__, |___/\__|_|  |_/_/\_\
        |___/


${AnsiColor.BRIGHT_MAGENTA}::${AnsiColor.BRIGHT_WHITE} Spring Boot Version: ${AnsiColor.BRIGHT_YELLOW}${spring-boot.version}${spring-boot.formatted-version} ${AnsiColor.BRIGHT_MAGENTA}::
::${AnsiStyle.FAINT} https://gitee.com/darkranger/spring-cloud-books/tree/master/spring-cloud-monitor-dashboard ${AnsiColor.BRIGHT_MAGENTA}::
${AnsiColor.BRIGHT_WHITE}